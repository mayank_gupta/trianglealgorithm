classdef TriangleAlgorithm1 < handle
    % Implementation of triangle algorithm 1
    
    properties
        p;
        pprime;
        %lastp;
        %lastpprime;
        counter;
        result;
        kernel;
        K;
        Kprime;
        Ktest;
        Kprimetest
        epsilon;
        KExV;
        KprimeExV;
        h;
        a;
        tolerance;
    end
    
    methods
        function TA1Obj = TriangleAlgorithm1 (K , Kprime, kernel, epsilon)
            TA1Obj.K = sparse(K);
            TA1Obj.Kprime = sparse(Kprime);
            TA1Obj.kernel = kernel;
            TA1Obj.counter = 0;
            TA1Obj.result = false;
            TA1Obj.p = sparse(K(1,:));
            TA1Obj.pprime = sparse(Kprime(1,:));
            TA1Obj.Ktest = sparse(K(1,:));
            TA1Obj.Kprimetest = sparse(Kprime(1,:));
            TA1Obj.epsilon = epsilon;
            TA1Obj.tolerance = 1e-10;
        end
        
        function con = isConverged (self)
            con = false;
            if self.kernel.dist2(self.Ktest, self.Kprimetest) <= self.epsilon*self.kernel.dist2(self.Ktest, self.p)
                con = true;
            elseif self.kernel.dist2(self.Ktest, self.Kprimetest) <= self.epsilon*self.kernel.dist2(self.pprime, self.Kprimetest)
                con = true;
            end
        end
        
        function res = solve (self)
            pivotFound = self.iterate();
            self.counter = 1;
            while ((pivotFound == true) && (self.isConverged() == false))
                pivotFound = self.iterate();
                self.counter = self.counter + 1;
            end
            res = pivotFound == true;
        end
        
        function pivotFound = iterate (self)
            pivotFound = false;
            nextp = self.getP();
            if isnan(nextp) == false
                self.Ktest = self.nearest(self.Kprimetest, self.Ktest, nextp);
                self.p = nextp;
                pivotFound = true;
                return;
            end
            nextpprime = self.getPPrime();
            if isnan(nextpprime) == false
                self.Kprimetest = self.nearest(self.Ktest, self.Kprimetest, nextpprime);
                self.pprime = nextpprime;
                pivotFound = true;
                return;
            end
        end
        
        function near = nearest (self, x, y, z)
            alpha = (self.kernel.dot(x, z) - self.kernel.dot(y, z) - self.kernel.dot(x, y) + self.kernel.dot(y, y));
            alpha = alpha / (self.kernel.dist2(y, z));
            if alpha >=0 && alpha <= 1
                near = self.kernel.scalarP(y, 1-alpha) + self.kernel.scalarP(z, alpha);
            else
                near = z;
            end
        end
        
        function pvt = getNextPivot (self, ch, p, test)
            for i=1: size(ch, 1)
                v = ch(i,:);
                if (self.kernel.dist2(p, v) >= self.kernel.dist2(test, v))
                    pvt = v;
                    return;
                end             
            end
            pvt = NaN;
        end
        
        function p = getP (self)
            p = self.getNextPivot(self.K, self.Ktest, self.Kprimetest);
        end
        
        function pprime = getPPrime (self)
            pprime = self.getNextPivot(self.Kprime, self.Kprimetest, self.Ktest);
        end
        
        function ex = getExtremePointK(self, ch, h)
            ex = ch(1,:);
            minD = self.kernel.dot(ex, h);
            for i=2: size(ch, 1)
                v = ch(i,:);
                d = self.kernel.dot(v, h);
                if d < minD
                    ex = v;
                    minD = d;
                end             
            end
        end
        
        function ex = getExtremePointKprime(self, ch, h)
            ex = ch(1,:);
            maxD = self.kernel.dot(ex, h);
            for i=2: size(ch, 1)
                v = ch(i,:);
                d = self.kernel.dot(v, h);
                if d > maxD
                    ex = v;
                    maxD = d;
                end             
            end
        end
        
        function E = getError (self, delta, deltabar)
            %ro = self.kernel.dot(self.Ktest, self.Kprimetest);
            %robar = (self.kernel.dot(self.h, self.KExV) - self.kernel.dot(self.h, self.KprimeExV))/sqrt(self.kernel.l2(self.h));
            E = abs(delta - deltabar);
        end
        
        function res = terminateCond (self, E)
            res = false;
            if (E - self.epsilon*self.kernel.dist(self.KExV, self.Ktest) <= self.tolerance)
                res = true;
            elseif (E - self.epsilon*self.kernel.dist(self.KprimeExV, self.Kprimetest) <= self.tolerance)
                res = true;
            end
        end
        
        function deltaV = getDeltaV (self)
            deltaV = (self.kernel.dot(self.KExV, self.h) - self.a)/sqrt(self.kernel.l2(self.h));
        end
        
        function deltaVprime = getDeltaVprime (self)
            deltaVprime = (self.kernel.dot(self.KprimeExV, self.h) - self.a)/sqrt(self.kernel.l2(self.h));
            %roBarV = sqrt(self.kernel.dist2(self.KprimeExV, self.h));
        end
        
        function delta = getDelta (self)
            delta = self.kernel.dist(self.Ktest, self.Kprimetest);
        end
        
        function deltaBar = getDeltaBar(self)
            deltaBar = (self.kernel.dot(self.h, self.KExV) - self.kernel.dot(self.h, self.KprimeExV))/sqrt(self.kernel.l2(self.h));
        end
        
        function computeHyperplane (self)
            self.h = self.Ktest - self.Kprimetest;
            self.KExV = self.getExtremePointK(self.K, self.h);
            self.KprimeExV = self.getExtremePointKprime(self.Kprime, self.h); 
            self.a = (self.kernel.dot(self.Ktest, self.Ktest) - self.kernel.dot(self.Kprimetest, self.Kprimetest))/2; 
        end
        
        function [delta, deltabar, err, res, deltaV, deltaVprime, ro, robar, Ev, Evprime] = testHyperplane(self)
            delta = self.getDelta();
            deltabar = self.getDeltaBar();
            err = self.getError(delta, deltabar);
            res = self.terminateCond(err);
            deltaV = self.getDeltaV();
            deltaVprime = self.getDeltaVprime();
            ro = self.kernel.dist(self.Ktest, self.KExV);
            robar = self.kernel.dist(self.Kprimetest, self.KprimeExV);
            Ev = (delta/2) - deltaV;
            Evprime = (delta/2) - deltaVprime;
        end
        
        function res = stage2 (self)
            self.computeHyperplane();
            [delta, deltabar, err, res, deltaV, deltaVprime, ro, robar, Ev, Evprime] = self.testHyperplane();
            while (res == false)    
                if (Ev > 0.5*self.epsilon*ro)
                    self.Kprimetest = self.nearest(self.Ktest, self.Kprimetest, self.KprimeExV);
                end
                if (Evprime > 0.5*self.epsilon*robar)
                    self.Ktest = self.nearest(self.Kprimetest, self.Ktest, self.KExV);
                end
                self.solve();
                self.computeHyperplane();
                [delta, deltabar, err, res, deltaV, deltaVprime, ro, robar, Ev, Evprime] = self.testHyperplane();
            end
        end
    end
    
end

