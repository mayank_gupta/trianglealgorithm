classdef TriangleAlgorithm1Test < matlab.unittest.TestCase
    properties
    end

    methods (Test)
        
        function testNearest (testCase)
            x = [2, 2];
            y = [0, 0];
            z = [4, 0];
            lk = LinearKernel;
            ta1 = TriangleAlgorithm1([x;y;z], [x;y;z], lk, 0.1);
            actSol = ta1.nearest(x, y, z);
            expSol = [2, 0];
            testCase.verifyEqual(actSol, expSol);
        end
        
        function testPivot (testCase)
            x = [ 0, 0; 0, 1; 1, 0; 1, 1];
            y = [ 0.5, 0; 2, 1; 3, 1; 3, 0];
            lk = LinearKernel;
            ta1 = TriangleAlgorithm1(x, y, lk, 0.1);
            actSol = ta1.getP();
            expSol = sparse([1, 0]);
            testCase.verifyEqual(actSol, expSol);
            
            actSol2 = ta1.getPPrime();
            expSol2 = NaN;
            testCase.verifyEqual(actSol2, expSol2);
            
        end
        
        function inseparableCHTest (testCase)
            x = [ 0, 0; 0, 1; 1, 0; 1, 1; 1.6, 0.5];
            y = [ 2, 0; 2, 1; 3, 1; 3, 0; 1.4, 0.5];
            lk = LinearKernel;
            ta1 = TriangleAlgorithm1(x, y, lk, 0.01);
            testCase.verifyEqual(ta1.solve(), true);
        end
        
        function separableCHTest (testCase)
            x = [ 0, 0; 0, 1; 1, 0; 1, 1];
            y = [ 2, 0; 2, 1; 3, 1; 3, 0];
            lk = LinearKernel;
            ta1 = TriangleAlgorithm1(x, y, lk, 0.01);
            testCase.verifyEqual(ta1.solve(), false);
        end
        
        function GetHyperplane (testCase)
            import matlab.unittest.constraints.RelativeTolerance;
            import matlab.unittest.constraints.IsEqualTo;
            x = [ 0, 0; 0, 1; 1, 0; 1, 1; 1.4, 0.5];
            y = [ 2, 0; 2, 1; 3, 1; 3, 0; 1.6, 0.5];
            lk = LinearKernel;
            ta1 = TriangleAlgorithm1(x, y, lk, 0.01);
            res = ta1.solve();
            testCase.verifyEqual(res, false);
            ta1.stage2();
            solerr = lk.dist(ta1.h, sparse([-0.2, 0]));
            testCase.verifyThat(solerr, IsEqualTo(sparse(0), 'Within', RelativeTolerance(10*eps)));
            testCase.verifyThat(ta1.a, IsEqualTo(sparse(-0.3), 'Within', RelativeTolerance(10*eps)));
        end
        
    end
    
end

