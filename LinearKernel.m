classdef LinearKernel
    % Function related to a linear kernel
    properties
    end
    methods
        function dotP = dot (~, a, b) 
            dotP = dot(a,b);
        end
        
        function d = dist2 (self, a, b)
            d = self.dot(a,a) + self.dot(b,b) - 2*self.dot(a,b);
        end
        
        function d = dist(self, a, b)
            d = sqrt(self.dist2(a, b));
        end
        
        function l = l2 (self, p)
            l = self.dot(p, p);
        end
        
        function d = pointPlaneDist (self, point, w, d)
            d = (self.dot(w, point) + d)/sqrt(self.l2(w));
        end
        
        function s = scalarP (~, a, k)
            s = k*a;
        end
    end
    
end

