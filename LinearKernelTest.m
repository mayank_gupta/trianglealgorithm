classdef LinearKernelTest < matlab.unittest.TestCase
    properties
        dim;
    end
    methods
        function testObj = LinearKernelTest()
            testObj.dim = 3;
        end
    end
    methods (Test)
        function testDotProduct (testCase)
            a = rand(1, testCase.dim);
            b = rand(1, testCase.dim);
            lk = LinearKernel;
            actSol = lk.dot(a, b);
            expSol = dot(a,b);
            testCase.verifyEqual(actSol, expSol);
        end
        
        function testL2Norm (testCase)
            p = rand(1, testCase.dim);
            lk = LinearKernel;
            actSol = lk.l2(p);
            expSol = sum(p.^2);
            testCase.verifyEqual(actSol, expSol);
        end
        
        function testEuclideanDistance (testCase)
            import matlab.unittest.constraints.RelativeTolerance;
            import matlab.unittest.constraints.IsEqualTo;
            a = rand(1, testCase.dim);
            b = rand(1, testCase.dim);
            lk = LinearKernel;
            actSol = lk.dist2(a, b);
            expSol = sum((a-b).^2);
            testCase.verifyThat(actSol, IsEqualTo(expSol, 'Within', RelativeTolerance(10*eps)));
        end
        
        function testPointPlaneDistance (testCase)
            point = [4, -4, 3];
            w = [2, -2, 5];
            d = 8;
            lk = LinearKernel;
            actSol = lk.pointPlaneDist(point, w, d);
            expSol = 6.79;
            testCase.verifyEqual(actSol, expSol);
        end
        
    end
    
end

